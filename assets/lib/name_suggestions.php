<?php 

$names = array(

	'A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. In electronic circuits, resistors are used to reduce current flow, adjust signal levels, to divide voltages, bias active elements, and terminate transmission lines, among other uses.', 
	'An inductor, also called a coil, choke or reactor, is a passive two-terminal electrical component that stores electrical energy in a magnetic field when electric current flows through it. An inductor typically consists of an insulated wire wound into a coil around a core.', 
	'A capacitor is a passive two-terminal electrical component that stores potential energy in an electric field. The effect of a capacitor is known as capacitance. While some capacitance exists between any two electrical conductors in proximity in a circuit, a capacitor is a component designed to add capacitance to a circuit.'

);

if (isset($_POST['suggestion'])){
$nameInput = strtolower($_POST['suggestion']);

	if (!empty($nameInput)) {
	foreach ($names as $name) {
		$name = strtolower($name);
		if (strpos($name, $nameInput) !== false) {
			echo ucwords($name) . '<br>';
			}
		}
	}
}

// echo $name;

 ?>