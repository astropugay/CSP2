<?php session_start(); ?>
<?php require 'connection.php' ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>Menu Page</title>
	<!-- imports bootstrap css -->
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<!-- imports custom css -->
	<link rel="stylesheet" type="text/css" href="assets/style.css">
	<!-- imports JQuery -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<!-- imports bootstrap js-->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- font awesome -->
    <script src="https://use.fontawesome.com/5051be9f40.js"></script>
</head>
<body>
	<?php require "partials/navbar.php" ?>


	<?php 
	$username = $_SESSION['username'];
	$total = 0;
	$totalquantity = 0;
	foreach ($_SESSION['cart'] as $index => $quantity) {
		$sql = "SELECT * FROM items WHERE id='$index'";
		$result = mysqli_query($conn,$sql);
		$row = mysqli_fetch_assoc($result);
		extract($row);

		$subtotal = $price * $quantity; 
		$total += $subtotal;
		$x = $quantity;
		$totalquantity = $totalquantity + $x;
		echo "<div>";
		echo "<img class='col-xs-4' src='$image' style='float:left'>";
		echo "<h3 name='name'>$name</h3><br>";
		echo "<h3 name='price'>$price</h3><br>";	
		echo "<div style='float:right'>
			<h4>$subtotal</h4>
			<form method='post' action='add-to-cart.php?index=$index'>
			<input type='number' name='change_quantity' min=1 value='$quantity'><br>
			<button class='btn btn-primary'>Change Quantity</button><br>
			<a href='removefromcart.php?index=$index'>
			<button type='button' class='btn btn-danger'>Remove From Cart</button></a>
			</div>";
		echo "</div>";
		echo "</form>";
		echo "<div style='clear:both'></div>";
	}
	echo "<center><h3>Total items: $totalquantity units</h3></center>";
	echo "<center><h3>Total price: Php $total</h3></center>";
	 ?>

	 <?php 

	if(isset($_POST['history'])){
		$username = $_POST['username'];
		$purchasedate = $_POST['date'];
		$sql = "INSERT INTO history (username,totalquantity,total,purchasedate) VALUES ('$username','$totalquantity','$total','$purchasedate')";
		$_SESSION['totalquantity'] = $_POST['totalquantity'];
		$_SESSION['total'] = $_POST['total'];
		$_SESSION['purchasedate'] = $_POST['date'];
		mysqli_query($conn,$sql);
	}
	  ?>
<center>
	<div style="width: 50%;">	
	 <form method="POST">
		Date: <input type="text" name="date" value="<?php echo date("Y/m/d") ?>"><br>
		Username: <input type="text" name="username" value="<?php echo $username ?>"><br>
		Total Items: <input type="text" name="totalquantity" value="<?php echo $totalquantity ?>"><br>
		Total: <input type="text" name="total" value="<?php echo $total ?>"><br>
		<button class="btn btn-success" type="submit" name="history" value="Check out">Confirm order</button>
		<a href="checkoutpage.php"><button name="id" class="btn btn-primary" type="button">Checkout</button></a>
	 </form>
</div>
</center>
	<?php require "partials/footer.php" ?>

</body>
</html>