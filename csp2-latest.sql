-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2018 at 08:30 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `csp2`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Resistor'),
(2, 'Capacitor'),
(3, 'Inductor');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `totalquantity` decimal(65,2) NOT NULL,
  `total` decimal(65,2) NOT NULL,
  `purchasedate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `username`, `totalquantity`, `total`, `purchasedate`) VALUES
(1, 'emil', '8.00', '1200.00', '2018-01-31'),
(2, 'emil', '3.00', '300.00', '2018-02-01'),
(3, 'emil', '3.00', '450.00', '2018-02-01'),
(4, 'goku', '5.00', '750.00', '2018-02-01'),
(5, 'goku', '8.00', '7600.00', '2018-02-01'),
(6, 'goku', '8.00', '7600.00', '2018-02-01'),
(7, 'goku', '11.00', '7450.00', '2018-02-01'),
(8, 'goku', '12.00', '8200.00', '2018-02-01'),
(9, 'goku', '12.00', '8200.00', '2018-02-01'),
(10, 'goku', '12.00', '8200.00', '2018-02-01'),
(11, 'goku', '14.00', '8900.00', '2018-02-01');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` decimal(65,2) NOT NULL,
  `category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `description`, `image`, `price`, `category_id`) VALUES
(1, 'Resistor 47kohm', 'Passive component', 'assets/img/resistor-47kohm.jpg', '100.00', 1),
(2, 'Resistor 51mohm', 'Passive component', 'assets/img/resistor-51mohm.jpg', '150.00', 1),
(3, 'Resistor 82ohm', 'Passive component', 'assets/img/resistor-82ohm.jpg', '200.00', 1),
(4, 'Capacitor 220pF', 'Passive component', 'assets/img/capacitor220pf.jpg', '100.00', 2),
(5, 'Capacitor 3300uF', 'Passive component', 'assets/img/capacitor3300uf.jpg', '150.00', 2),
(6, 'Capacitor 4700uF', 'Passive component', 'assets/img/capacitor4700uf.jpg', '150.00', 2),
(7, 'Inductor C-Core', 'Passive', 'assets/img/inductor-c-core.jpg', '100.00', 3),
(8, 'Inductor Dimmer', 'Passive', 'assets/img/inductor-dimmer.jpg', '150.00', 3),
(9, 'Inductor Traftor', 'Passive', 'assets/img/inductor-traftor.jpg', '150.00', 3),
(10, 'Inductor Toroidal', 'Passive', 'assets/img/inductor-toroidal.jpg', '350.00', 3),
(11, 'Resistor Snubber', 'Passive', 'assets/img/resistor-snubber.jpg', '150.00', 1),
(12, 'Resistor Force Sensitive', 'Passive', 'assets/img/resistor-forcesensitive.jpg', '750.00', 1),
(13, 'Capacitor 68uF', 'Passive', 'assets/img/capacitor68uf.png', '750.00', 2),
(14, 'inductor Air Coil', 'Passive', 'assets/img/inductor-aircoil.jpg', '950.00', 3),
(15, 'Capacitor CDU', 'Passive', 'assets/img/capacitor-cdu.jpg', '950.00', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `role`) VALUES
(1, 'admin@yahoo.com', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin'),
(4, 'goku@yahoo.com', 'goku', '7dbd464b96cc2897507be8a475926dbe173ad452', 'regular'),
(5, 'emil@yahoo.com', 'emil', 'ae80d870eb40a8fd7c256c0ec3faf2ba511ba134', 'regular');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
