<?php session_start(); ?>
<?php require 'connection.php' ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>Edit Page</title>
	<!-- imports bootstrap css -->
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<!-- imports custom css -->
	<link rel="stylesheet" type="text/css" href="assets/style.css">
	<!-- imports JQuery -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<!-- imports bootstrap js-->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- font awesome -->
    <script src="https://use.fontawesome.com/5051be9f40.js"></script>
</head>
<body>
	<?php require "partials/navbar.php" ?>

	<?php 
	$id = $_GET['id'];
if(isset($_POST['save'])){
	$id = $_POST['id'];
	$name = $_POST['name'];
	$description = $_POST['description'];
	$image = $_POST['image'];
	$price = $_POST['price'];
	$category_id = $_POST['category_id'];
	$edit = "UPDATE items SET id='$id',name='$name',description='$description',image='$image',price='$price',category_id='$category_id' WHERE id = $id";
	mysqli_query($conn,$edit);
}

	$sql = "SELECT * FROM items WHERE id = $id";
	$result = mysqli_query($conn,$sql);
	$row = mysqli_fetch_assoc($result);
	extract($row);	
	?>
	<!-- <div> -->
	<form method="POST">
		ID: <input type="text" name="id" value="<?php echo $id ?>"><br>
		Name: <input type="text" name="name" value="<?php echo $name ?>"><br>
		Description: <input type="text" name="description" value="<?php echo $description ?>"><br>
		Image URL: <input type="text" name="image" value="<?php echo $image ?>"><br>
		Price: <input type="text" name="price" value="<?php echo $price ?>"><br>
		Category_id: <input type="text" name="category_id" value="<?php echo $category_id ?>"><br>
		<input type="submit" name="save" value="save">
	</form>
	<!-- </div> -->
	<hr>


	<?php require "partials/footer.php" ?>

</body>
</html>