<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>Home Page</title>
	<!-- imports bootstrap css -->
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<!-- imports custom css -->
	<link rel="stylesheet" type="text/css" href="assets/style.css">
	<!-- imports JQuery -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<!-- imports bootstrap js-->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- font awesome -->
    <script src="https://use.fontawesome.com/5051be9f40.js"></script>
</head>
<body>
	<?php require "partials/navbar.php" ?>
	<input type="text" name="nameInput" id="nameInput" placeholder="Help! (Input: Resistor,Inductor or Capacitor)">
	<span id="namesSuggested"></span>
	<?php require "partials/carousel.php" ?>
	<div class="circuit"></div>
	<script type="text/javascript">
				$(document).ready(function(){
			$('#nameInput').keyup(function() {
				var name = $('#nameInput').val();
				// console.log(name);
				$.post('assets/lib/name_suggestions.php', 
					{'suggestion' : name},
					function(data, status) {
						// console.log(name);
					$('#namesSuggested').html(data);
				});
			});
		});
	</script>

	<?php require "partials/footer.php" ?>

</body>
</html>