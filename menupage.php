<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>Menu Page</title>
	<!-- imports bootstrap css -->
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<!-- imports custom css -->
	<link rel="stylesheet" type="text/css" href="assets/style.css">
	<!-- imports JQuery -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<!-- imports bootstrap js-->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- font awesome -->
    <script src="https://use.fontawesome.com/5051be9f40.js"></script>
</head>
<body>
	<?php require "partials/navbar.php" ?>
	<?php require "partials/carousel.php" ?>

<?php 
	require 'connection.php';
	$filter = isset($_GET['category']) ? $_GET['category'] : 'All';

	echo "<div class='select'><form><select name='category'><option>All</option>";
	$sql = "SELECT * FROM categories";
	$result = mysqli_query($conn,$sql);
	// $sql2 = "SELECT * FROM items";
	// $result2 = mysqli_query($conn,$sql2);
	// $row 
	while($row = mysqli_fetch_assoc($result)){
		$id = $row['id'];
		$category = $row['name'];
		$description =$row['description'];
		echo $filter == $id ? "<option selected value='$id'>$category</option>" : "<option value='$id'>$category</option>";
	}
	echo "</select>";
	echo "<button>Search</button></form></div>";

	$sql = "SELECT * FROM items";
	$result = mysqli_query($conn,$sql);


	echo "<div class='row'>";
	while($item = mysqli_fetch_assoc($result)) {
		$index = $item['id'];
		$description = $item['description'];

		if($filter == 'All' || $item['category_id'] == $filter){
			echo "<div class='col-xs-12 col-sm-5 col-md-2 menu'><img src='".$item['image']."'>";
			echo "<h5>".$item['name']."</h5>";
			echo "Price: Php".$item['price']."<br>";
			if(isset($_SESSION['username']) && $_SESSION['role'] == 'admin'){
				echo "<a href='editpage.php?id=$index'><button class='btn btn-primary' name='edit button'>Edit</button></a>";
				echo "<a href='deleteitem.php?id=$index'><button class='btn btn-danger'>Delete</button></a>";
			}
			else if(isset($_SESSION['username']) && $item['id'] == 1){
			echo "<div class='soldout'>";
				echo "<form method='POST' action='add-to-cart.php?index=$index'>";
					echo "<input class='inputcart' type='number' name='quantity' min=0>";
					echo "<button class='btn btn-success' disabled>Add to Cart</button>";
					echo "<button type='button' class='btn btn-primary' data-container='body' data-toggle='popover' data-placement='top' data-content='$description'>Description</button>";
				echo "</form>";
			echo "</div>";
			} else if(isset($_SESSION['username'])) {
				echo "<form method='POST' action='add-to-cart.php?index=$index'>";
					echo "<input class='inputcart' type='number' name='quantity' min=0>";
					echo "<button class='btn btn-success'>Add to Cart</button>";
					echo "<button type='button' class='btn btn-primary' data-container='body' data-toggle='popover' data-placement='top' data-content='$description'>Description</button>";
				echo "</form>";
			}
			echo "</div>"; 
		}
	}
	echo "</div>";	



 ?>
<script type="text/javascript">
$(function () {
  $('[data-toggle="popover"]').popover()
})
 </script>

	<?php require "partials/footer.php" ?>

</body>
</html>