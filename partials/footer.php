 <footer class="footer"><p>
 	Astro Pugay &copy; 2018
        <a href="https://facebook.com"><i class="fa fa-facebook-official fa-lg" aria-hidden="true"></i></a>
        <a href="https://instagram.com"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a>
        <a href="https://twitter.com"><i class="fa fa-twitter fa-lg" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-info fa-lg" aria-hidden="true" type='button' data-container='body' data-toggle='popover' data-placement='top' data-content='Disclaimer: No copyright infringement is intended.This is only for educational purposes and not for profit. Some asset/s used are not owned by the developer/s unless otherwise stated; the credit goes to the owner.'></i></a>
 </footer>
 <script type="text/javascript">
$(function () {
  $('[data-toggle="popover"]').popover()
})
 </script>