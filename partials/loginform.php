<form method="POST" action="">
  <div class="form-group">
    <label for="username">Username:</label>
    <input type="text" class="form-control" id="username" name="username">
  </div>
  <div class="form-group">
    <label for="pwd">Password:</label>
    <input type="password" class="form-control" id="pwd" name="password">
  </div>
  <div class="checkbox">
    <label><input type="checkbox"> Remember me</label>
  </div>
  <button name="log_in" type="submit" class="btn btn-success">Log in</button>
</form>

<?php 
require "connection.php";
if (isset($_POST['log_in'])){
  $username = mysqli_real_escape_string($conn,$_POST['username']);
  $password = sha1(mysqli_real_escape_string($conn,$_POST['password']));

  if(empty($username) || empty($password)){
    header("Location: loginpage.php?login=empty");
    exit();
  }else {
    $sql = "SELECT * FROM users WHERE username = '$username' ";
    $result = mysqli_query($conn,$sql);
    $resultCheck = mysqli_num_rows($result);
    if ($resultCheck < 1) {
      header("Location: loginpage.php?login=usernamedoesnotexist");
      exit();
    } else {
      if ($row = mysqli_fetch_assoc($result)){
        // $_SESSION['role'] = $row['role'];
        if ($password != $row['password']) {
          header("Location: loginpage.php?login=wrongpassword");
          exit();
        } elseif ($username == $row['username'] && $password == $row['password']) {
               $_SESSION['username'] = $username;
               $_SESSION['role'] = $row['role'];
               header("Location: index.php?login=success");
          exit();
        }
      }
    }  
  }
}
?>