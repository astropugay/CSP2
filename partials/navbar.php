
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">RLC Shop</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="index.php">Home</a></li>
      <li><a href="menupage.php">Menu</a></li>
    </ul>

    <ul class="nav navbar-nav navbar-right">
     <?php echo isset($_SESSION['username']) ? '<li><a href="userpage.php"><span class="glyphicon glyphicon-user"></span> Welcome! '.$_SESSION['username'].'</a></li>' : '' ?>

     <?php echo isset($_SESSION['username']) ? '' : '<li><a href="signuppage.php"><span class="glyphicon glyphicon-pencil"></span> Sign Up</a></li>' ?>

     <?php echo isset($_SESSION['username']) ? '' : '<li><a href="loginpage.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>' ?>

     <?php echo isset($_SESSION['cart']) ? '<li><a href="cartpage.php"><span class="glyphicon glyphicon-shopping-cart"></span> Cart</a></li>' : '' ?>

     <?php 
     if(isset($_SESSION['role']) && $_SESSION['role'] == 'admin') {
      echo '<li><a href="adminpage.php"><span class="glyphicon glyphicon-remove"></span> Delete Users</a></li>';
      echo '<li><a href="additempage.php"><span class="glyphicon glyphicon-plus"></span> Add items</a></li>';
      echo '<li><a href="historypage.php"><span class="glyphicon glyphicon-book"></span> CLient History</a></li>';
     } else {
      echo '';
     }
     ?>
     
     <?php echo isset($_SESSION['username']) ? '<li><a href="logoutpage.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>' : '' ?>
    </ul>
<!--     <form class="navbar-form navbar-left" action="/action_page.php">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Search">
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form> -->
  </div>
</nav>